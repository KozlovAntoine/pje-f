#!/bin/bash
for i in 1 2 3 4
do 
	temp=`/opt/vc/bin/vcgencmd measure_temp`
	volt=`/opt/vc/bin/vcgencmd measure_volts`
	#clock=`/opt/vc/bin/vcgencmd measure_clock`
	DATE=`date +"%a %d %H:%M:%S"`
	echo $DATE '[CPU]' $temp $volt >> cpu-temp.log
	sleep 15
done
