# 1. PJE - F
## 1.1 Auteur : Kozlov Antoine

# 2. Procedure d'installation
## 2.1. Carte sd
Démonter la carte sd
```
sudo umount /chemin/vers/cartesd
```

[Télécharger l'iso](https://sourceforge.net/projects/xbian/files/release/XBian_Latest_rpi4.img.gz/download)

Installation ISO sur carte sd
```
sudo dd bs=4M if=chemin/vers/fichier.iso of=/chemin/vers/cartesd status=progress oflag=sync
```

## 2.2. Clé ssh
Changer mot de passe de connection : 
```
Paramètre -> XBian -> System 
    -> XBian password
    -> Root password
```

Création de la clé SSH
```
ssh-keygen
```

Copie de la clé RSA vers la machine distante
```
ssh-copy-id -i /chemin/id_rsa.pub xbian@ip
```

Changer paramètre SSH
```
nano /etc/ssh/sshd_config
# Mettre à jour le fichier
PasswordAuthentication no
PermitRootLogin no
# Enregistrer le fichier puis redemarrer le service ou la machine
systemctl restart sshd.service
```

## 2.3. Plugin IPTV
Se rendre dans Paramètre -> Addon-ons -> My Addon-ons -> PVR Clients -> PVR IPTV Simple Client

Activer l'add-on puis le configurer :
```
Location : remote path
M3U playlist URL : https://iptv-org.github.io/iptv/countries/fr.m3u
OK -> Redémarrer la machine avec sudo reboot
```

# 3. Spécialisation de l'interface
## 3.1. Fichier XML Dédiées
Chemin vers le dossier XML : /usr/local/share/kodi/addons/skin.estuary/xml

Menu : Home.xml

Enlever `<include> background <include>` + `<control multiimage>` permet d'enlever l'image de fond

Dans `<include>OpenClose_Right</include>` :
 - enlever tous les `<control id=unNombre>`sauf 
   - movies id=5000
   - tvshows id=6000
   - livetv id=12000

Pour afficher le trailer :
```xml
<control type="group" id="5001">
	<include>WidgetGroupListCommon</include>
	<pagecontrol>5010</pagecontrol>
    <!--Ajouter la partie en dessous-->
    <control type="videowindow" id="2">
        <description>My first video control</description>
        <left>500</left>
        <top>10</top>
        <width>800</width>
        <height>500</height>
        <aspectratio>scale</aspectratio>
        <visible>Player.hasMedia</visible>
    </control>
    <!--...-->
```

Changer le logo en haut à gauche : `<texture>special://xbmc/media/vendor_logo.png</texture>`

Enlever les boutons sauf settings dans : `<control type="grouplist" id="700">`

Ajouter le lancement du trailer : `<control type="fixedlist" id="9000">`
```xml
<!--Lance le trailer quand on sur la page de films-->
<onfocus condition="String.IsEqual(Container(9000).ListItem.Property(id),movies) + !Player.hasMedia">PlayMedia("/media/Element/trailer.mp4", 1)</onfocus>
<!--Arrete le lecteur lorsqu'on change de page-->
<onfocus condition="String.IsEqual(Container(9000).ListItem.Property(id),tvshows) | String.IsEqual(Container(9000).ListItem.Property(id),livetv)">PlayerControl(stop)</onfocus>
```

# 4. Scripts de déploiement
## 4.1. Suivi de température
Script bash qui récupère la température toutes les 15 secondes
```bash
#!/bin/bash
for i in 1 2 3 4
do 
	temp=`/opt/vc/bin/vcgencmd measure_temp`
	volt=`/opt/vc/bin/vcgencmd measure_volts`
	#clock=`/opt/vc/bin/vcgencmd measure_clock`
	DATE=`date +"%a %d %H:%M:%S"`
	echo $DATE '[CPU]' $temp $volt >> cpu-temp.log
	sleep 15
done
```
On lance `chmod +x log_temp.sh` pour pouvoir l'executer ensuite

Puis lancer le script toutes les minutes avec `crontab -e` : 
```bash
#Fin du fichier
* * * * * ./log_temp.sh
```

## 4.2. Extraction automatique des trailers
```bash
#!/bin/bash
root='/media/Element/Films'
for d in $root/* ; do
    cd $d
    output_file="$(basename $PWD)-Trailer"
    test_file="$(basename $PWD)-Trailer.mkv"
    trailer="$(basename $PWD) trailer vf"
    if test -f "$test_file";
    then
        echo Le fichier $test_file existe déjà
    else
        echo Début du téléchargement de \'$test_file\'
        youtube-dl "ytsearch1:$trailer" --output "$output_file" --recode-video mkv
        echo Fin du téléchargement, retour dans $root
        cd ..
    fi
done
```
On le lance à chaque démarrage avec `crontab -e`
```bash
#Fin du fichier
@reboot ./download_trailer
```

## 4.3. Deploiement
Script de déploiement :
```bash
#!/bin/bash
Nom_DuFilm="$1"
ip=#on recupere l'ip depuis une base
while [-z "${ip}"]; then
#la commande en dessous permet de commencer un transfert ou de reprendre s'il y a eu une interruption
rsync -P -e ssh -r "/media/Element/Film/$Nom_DuFilm/" "xbian@$ip:/media/Element/" 
#on envoie une requete au serveur pour dire que le film
#puis on envoie la commande pour que le nouveau serveur transmet un nouveau film
ssh -t "xbian@$ip" "./deploiement $Nom_DuFilm"
ip=#on recupere l'ip depuis une base
fi
```
On lance `chmod +x deploiement` pour pouvoir l'executer ensuite
# 5. Benchmarks
## 5.1. Température
![Température](graphique_temperature.PNG)
> On peut s'apercevoir que la température oscille entre **37°C et 86°C**
>
> En moyenne c'est entre **60°C et 70°C**
> 
> Lorsque les trailers sont convertis en fichiers _MKV_ la température stagne vers 85°C car le processeur baisse sa cadence (_à partir de 80°C_).

**On peut abandonner le ventilateur**, lorsque notre produit est envoyé les trailers sont déjà dessus. Les trailers sont téléchargées à chaque nouveau film mais cela reste sur un délai assez court.

En utilisation **normal**, le processeur est entre 60°C et 70°C. A ce niveau le processeur à la bonne cadence, il ne perd donc pas de puissance.

## 5.2. Temps de distribution
Fibre 1Gbits/s soit 125Mo/s (débit théorique) soit 16 secondes par film de 2Go.
ADSL 8Mb/s soit 1Mo/s (débit théorique) soit (2048 sec) 34 minutes par film de 2Go.

Si on commence par 1 machine et qu'on déploie sur 15M de machine, il nous faut faire 24 étapes. 1 -> 2 -> 4 -> 8 -> 2^n -> 2^24

> Dans le **meilleur des cas** si tous les utilisateurs ont la fibre cela prend 16x24 = 384 secondes soit 6m24sec pour que tous les utilisateurs aient le nouveau film

> Dans le **pire des cas** si tous les utilisateurs ont l'adsl cela prend 2048x24 = 49 152 secondes soit 13h39min pour que tous les utilisateurs aient le nouveau film

> En **moyenne** si la moitié des utilisateurs ont la fibre et l'autre l'adsl cela prend ((2048+16) / 2)x24 = 24 768 secondes soit 6h52min pour que tous les utilisateurs aient le nouveau film
